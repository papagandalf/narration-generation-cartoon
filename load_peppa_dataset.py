import os
import json
import jsons
from dataclasses import dataclass
from typing import List

from tqdm import tqdm
import numpy as np


@dataclass
class NarratorSplit:
    context: dict
    narration: dict
    split_id: int
    dialogue_tokenized: List[str]
    narration_tokenized: List[str]


@dataclass
class Episode:
    id: str
    title: str
    plot: str
    narrator_splits: List[NarratorSplit]


def read_dataset(dataset_base_dir):
    episode_dataset = dict()
    for x in tqdm(range(1, 210)):
        episode_object = json.load(
            open(os.path.join(dataset_base_dir, "episodes", f"ep_{x}.json"), "r")
        )
        episode_object = jsons.load(episode_object, Episode)
        episode_features = np.load(
            os.path.join(dataset_base_dir, "episodes", f"ep_{x}.npz")
        )
        mfcc, resnet, vggish = (
            episode_features["mfcc"],
            episode_features["resnet"],
            episode_features["vggish"],
        )
        # get per token features
        for _n_s in episode_object.narrator_splits:
            for _t in _n_s.context["tokenized"] + _n_s.narration["tokenized"]:
                for feature_class, feature_matrix in [
                    ("mfcc", mfcc),
                    ("resnet", resnet),
                    ("vggish", vggish),
                ]:
                    _t[feature_class] = feature_matrix[_t["token_id"]]
        data_splits = json.load(
            open(os.path.join(dataset_base_dir, "dataset_splits.json"))
        )
        episode_dataset[episode_object.id] = episode_object
    return episode_dataset, data_splits


def load_episode_splits(episode_dataset, data_splits):
    train = [episode_dataset[x] for x in data_splits["episode_splits"]["train"]]
    val = [episode_dataset[x] for x in data_splits["episode_splits"]["val"]]
    test = [episode_dataset[x] for x in data_splits["episode_splits"]["test"]]
    return train, val, test


def load_narration_splits(episode_dataset, data_splits):
    narration_splits_dataset = {
        _ns.split_id: _ns
        for _ep_id, _ep in episode_dataset.items()
        for _ns in _ep.narrator_splits
    }
    train = [
        narration_splits_dataset[x]
        for x in data_splits["narration_splits_splits"]["train"]
    ]
    val = [
        narration_splits_dataset[x]
        for x in data_splits["narration_splits_splits"]["val"]
    ]
    test = [
        narration_splits_dataset[x]
        for x in data_splits["narration_splits_splits"]["test"]
    ]
    return train, val, test


if __name__ == "__main__":
    EPISODE_DATASET, DATA_SPLITS = read_dataset("./data")
    E_TRAIN, E_VAL, E_TEST = load_episode_splits(EPISODE_DATASET, DATA_SPLITS)
    N_TRAIN, N_VAL, N_TEST = load_narration_splits(EPISODE_DATASET, DATA_SPLITS)
