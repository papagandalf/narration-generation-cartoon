# narration generation for cartoon videos

## load data

* Unzip the downloaded dataset in `data`.
* Use the `load_peppa_dataset.py` to load the data splits for narration 
timing (episode splits) or narration generation (narration splits).